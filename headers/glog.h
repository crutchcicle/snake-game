#ifndef GLOG_H_INCLUDED
#define GLOG_H_INCLUDED

#include <GL/gl.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>

#define FIELD_SIZE 64
#define GRID_SIZE 10.0f
#define LINE_WIDTH 1.0
	
typedef struct SNode SNode;
typedef struct Head Head;

void Game(void);
void Reshape(int, int);
void Keyboard(unsigned char, int, int);
void SKeyboard(int, int, int);
void Draw(void);
void RenderPrintf3fNsVarg(char*, float, float, float, void*, size_t, ...);
void Movement(int);
void TailGrow(int, int);
void drawCircle3f(float, float, float, float);

#endif