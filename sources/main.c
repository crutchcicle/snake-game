#include "../headers/glog.h"
#include <time.h>

char speed = 40;

int main(int argc, char** argv) {
	srand(time(NULL));
	glutInit(&argc, argv); 
	glutInitWindowPosition(100,100);
	glutInitWindowSize(320,320);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutGameModeString("2000x2000:16");
	glutEnterGameMode();
	glutDisplayFunc(Game);
	glutIdleFunc(Game);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(SKeyboard);
	glutTimerFunc(speed, Movement, 0);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glutMainLoop();
	return 0; 
}