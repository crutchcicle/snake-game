#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "../headers/glog.h"

#define drawSquare(topLeftX, topLeftY, bottomRightX, bottomRightY)\
glVertex3f(topLeftX, topLeftY, 0);\
glVertex3f(bottomRightX, topLeftY, 0);\
glVertex3f(bottomRightX, bottomRightY, 0);\
glVertex3f(topLeftX, bottomRightY, 0);

extern char speed;

struct SNode {
	SNode *next;
	uint8_t x;
	uint8_t y;
}; 

enum Directions { UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3 } headDirection = RIGHT;
enum State { PAUSE = 0, GAMEPLAY = 1, GAMEOVER = 2 } act = GAMEPLAY;

int W;
int H;
unsigned char Y = 0;
unsigned char X = 1;
unsigned char appleX = FIELD_SIZE - 1;
unsigned char appleY = FIELD_SIZE - 1;
char dirChange = 1;
unsigned long long score = 0;
SNode *tail;

void drawCircle3f(float xc, float yc, float zc, float r) {
	float angle = 0.0;
	glBegin(GL_POLYGON);
	for (int i = 0; i < 360; i++) {
		glVertex3f(xc + cos(angle) * r, yc + sin(angle) * r, zc);
		angle += 2 * M_PI / 360;
	}
	glEnd();
}

void Game(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glLineWidth(LINE_WIDTH);
	if (!tail) {
		tail = (SNode*)malloc(sizeof(SNode));
		tail->x = 0;
		tail->y = 0;
		tail->next = NULL;
	}
	glColor3ub(0xff, 0x0, 0x0);
	switch (act) {
		case GAMEPLAY : Draw(); break;
		case GAMEOVER : RenderPrintf3fNsVarg( "GAMEOVER",
																					W/2.0 - 5 * 12,
																					H/2.0,
																					0.0f,
																					GLUT_BITMAP_TIMES_ROMAN_24,
																					10); break;
		case PAUSE 		: RenderPrintf3fNsVarg( "PAUSE",
																					W/2.0 - 2 * 12,
																					H/2.0,
																					0.0f,
																					GLUT_BITMAP_TIMES_ROMAN_24,
																					10); break;
	}
	glutSwapBuffers();
}

void Reshape(int w, int h) {
	W = w;
	H = h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	glOrtho(0.0f, w, h, 0.0f, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
}

void Draw(void) {
	glColor3ub(0xff, 0xff, 0xff);
	glBegin(GL_LINE_LOOP);
		glVertex3f( GRID_SIZE - LINE_WIDTH,
								GRID_SIZE - LINE_WIDTH,
								0.0);
		glVertex3f( GRID_SIZE + GRID_SIZE * FIELD_SIZE + LINE_WIDTH,
								GRID_SIZE - LINE_WIDTH,
								0.0);
		glVertex3f( GRID_SIZE + GRID_SIZE * FIELD_SIZE + LINE_WIDTH,
								GRID_SIZE + GRID_SIZE * FIELD_SIZE + LINE_WIDTH, 
								0.0);
		glVertex3f( GRID_SIZE - LINE_WIDTH, 
								GRID_SIZE + GRID_SIZE * FIELD_SIZE + LINE_WIDTH,
								0.0);
	glEnd();
	glBegin(GL_QUADS);
		glColor3ub(0x40, 0xA8, 0x00);
		drawSquare( GRID_SIZE + GRID_SIZE * X,
								GRID_SIZE + GRID_SIZE * Y,
								GRID_SIZE * 2.0 + GRID_SIZE * X,
								GRID_SIZE * 2.0 + GRID_SIZE * Y);
		SNode* piece = tail;
		while (piece != NULL) {
			drawSquare( GRID_SIZE + GRID_SIZE * piece->x,
									GRID_SIZE + GRID_SIZE * piece->y,
									GRID_SIZE * 2.0 + GRID_SIZE * piece->x,
									GRID_SIZE * 2.0 + GRID_SIZE * piece->y);
			piece = piece->next;
		}
	glEnd();
	glColor3f(0xff, 0x0, 0x0);
	drawCircle3f( GRID_SIZE * 1.5 + GRID_SIZE * appleX, 
							  GRID_SIZE * 1.5 + GRID_SIZE * appleY,
							  0.0, 
							  GRID_SIZE / 2.0 );
	glColor3ub(0x40, 0xA8, 0x00);
	RenderPrintf3fNsVarg( "SCORE -> %d",
											  GRID_SIZE * (FIELD_SIZE + 2),
											  100,
												0,
												GLUT_BITMAP_TIMES_ROMAN_24,
											  64,
											  score );
}

void Keyboard(unsigned char keyCode, int x, int y) {
	switch (keyCode) {
		case 27 :
			exit(0);
		break;
		case 32 :
			switch (act) {
				case PAUSE :
					act = GAMEPLAY;
				break;
				case GAMEPLAY :
					act = PAUSE;
				break;
				case GAMEOVER :
					exit(0);
				break;
			}
		break;
	}
}

void SKeyboard(int keyCode, int x, int y) {
	if (!dirChange || !act)
		return;
	switch (keyCode) {
		case GLUT_KEY_LEFT :
			if (headDirection != RIGHT && headDirection != LEFT) {
				headDirection = LEFT;
				dirChange = 0;
			}
		break;
		case GLUT_KEY_UP :
			if (headDirection != DOWN && headDirection != UP) {
				headDirection = UP;
				dirChange = 0;
			}
		break;
		case GLUT_KEY_RIGHT :
			if (headDirection != LEFT && headDirection != RIGHT) {
				headDirection = RIGHT;
				dirChange = 0;
			}
		break;
		case GLUT_KEY_DOWN :
			if (headDirection != UP && headDirection != DOWN) {	
				headDirection = DOWN;
				dirChange = 0;
			}
		break;
		case GLUT_KEY_F1 :
			glutIconifyWindow();
		break;
	}
}

void RenderPrintf3fNsVarg(char* format, float x, float y, float z, void* font, size_t n, ...) {
	char *cstring = (char*)malloc(n * sizeof(char));
	if (!cstring)
		return;
	glRasterPos3f(x, y, z);
	va_list args;
	va_start(args, format);
	vsnprintf(cstring, n, format, args);
	va_end(args);
	size_t l = strlen(cstring);
	for (size_t i = 0; i < l; i++)
		glutBitmapCharacter(font, cstring[i]);
}

void Movement(int value) {
	if (act == GAMEOVER || act == PAUSE) {
		glutTimerFunc(speed, Movement, 0);
		return;
	}
	SNode *piece = tail;
	short tempX = piece->x;
	short tempY = piece->y;
	while (piece->next != NULL) {
		piece->x = piece->next->x;
		piece->y = piece->next->y;
		piece = piece->next;
	}
	piece->x = X;
	piece->y = Y;
	switch (headDirection) {
		case LEFT  : X = --X % FIELD_SIZE; break;
		case UP    : Y = --Y % FIELD_SIZE; break;
		case RIGHT : X = ++X % FIELD_SIZE; break;
		case DOWN  : Y = ++Y % FIELD_SIZE; break;
	}
	dirChange = 1;
	if (X == appleX && Y == appleY) {
		TailGrow(tempX, tempY);
		appleX = rand() % FIELD_SIZE;
		appleY = rand() % FIELD_SIZE;
		++score;
	}
	SNode *tempPiece = tail;
	while (tempPiece != NULL) {
		if (X == tempPiece->x && Y == tempPiece->y) {
			act = GAMEOVER;
			break;
		}
		tempPiece = tempPiece->next;
	}
	glutTimerFunc(speed, Movement, 0);
}

void TailGrow(int x, int y) {
	SNode* temp = (SNode*)malloc(sizeof(SNode));
	temp->x = x;
	temp->y = y;
	temp->next = tail;
	tail = temp;
}